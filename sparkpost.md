# Sparkpost

### Sending Domain

- Create a sending domain.

  - Sending domains documentation - https://www.sparkpost.com/docs/getting-started/setting-up-domains/
  - Add a new sending domain.
  - Configure the domain provider to send with SparkPost.
    1. Add TXT record for DKIM
    2. Add CNAME record for Bounce
  - Confirm that the sending domain was successfully verified.

- Create an API key
